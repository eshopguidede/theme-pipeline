
const fs = require('fs');
const { promisify } = require('util');
const chalk = require('chalk');
const yaml = require('js-yaml');

const cmd = require('../commands/cmd.js');
const Adapter = require('./Adapter');

const writeFileAsync = promisify(fs.writeFile);

module.exports = class SlateAdapter extends Adapter {
  
  constructor(options) {
    super(options);
    this.configPath = './src/config/';
    this.localesPath = './src/locales';
  }

  async deploy() {
    const { shopNames, apiKeys, passwords, branch } = this.options;

    // Write email to config file to skip slate tools analytics prompt
    await writeFileAsync("./.env", 'SLATE_USER_EMAIL=setup@eshop-guide.de');

    for(const [idx, name] of shopNames.entries()) {
      console.log(chalk.inverse('Prepare deployment to ' + shopNames[idx]));

      await this.shopifyHelper.init({
        shopName: shopNames[idx],
        apiKey: apiKeys[idx],
        password: passwords[idx],
        branch
      });

      // settings_data.json
      await this.writeConfig();

      // locales
      await this.mergeLocales();

      // Setup environment config
      const targetTheme = await this.shopifyHelper.getTargetTheme();
      const ignores = fs.existsSync('.themeignore') ? ['.themeignore'] : [];

      fs.writeFileSync('config.yml', yaml.safeDump({
        deployment: {
          password: passwords[idx],
          theme_id: targetTheme.id,
          store: `${shopNames[idx]}.myshopify.com`,
          ignores
        }
      }));

      console.log(chalk.bgGreen.white('Deploying to theme #' + targetTheme.id));
      
      await cmd('npx', ['slate',  'deploy', '--env=deployment'], true);
      
      await this.afterDeployment();
    }

    console.log(chalk.bgGreen.white('🔥🔥🔥 Deployment successful 🔥🔥🔥'));
  }
}
