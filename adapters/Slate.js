
const fs = require('fs');
const { promisify } = require('util');
const chalk = require('chalk');
const dedent = require('dedent-js');

const cmd = require('../commands/cmd.js');
const Adapter = require('./Adapter');

const writeFileAsync = promisify(fs.writeFile);

module.exports = class SlateAdapter extends Adapter {
  
  constructor(options) {
    super(options);
    this.configPath = './dist/config/';
    this.localesPath = './dist/locales';
  }

  async deploy() {
    const { shopNames, apiKeys, passwords, branch } = this.options;

    console.log(chalk.inverse('Build theme...'));

    // Write email to config file to skip slate tools analytics prompt
    await writeFileAsync("./.env", 'SLATE_USER_EMAIL=setup@eshop-guide.de');
    
    // Build once, deploy multiple times
    await cmd('npx', ['slate-tools',  'build', '--skipPrompts']);

    for(const [idx, name] of shopNames.entries()) {
      console.log(chalk.inverse('Prepare deployment to ' + shopNames[idx]));

      await this.shopifyHelper.init({
        shopName: shopNames[idx],
        apiKey: apiKeys[idx],
        password: passwords[idx],
        branch
      });

      // settings_data.json
      await this.writeConfig();

      // locales
      await this.mergeLocales();

      // Setup environment config
      const targetTheme = await this.shopifyHelper.getTargetTheme();

      await writeFileAsync("./.env", dedent(`
        SLATE_STORE=${shopNames[idx]}.myshopify.com
        SLATE_PASSWORD=${passwords[idx]}
        SLATE_THEME_ID=${targetTheme.id}
        SLATE_USER_EMAIL=setup@eshop-guide.de
        SLATE_IGNORE_FILES=
      `));

      console.log(chalk.bgGreen.white('Deploying to theme #' + targetTheme.id));
			await cmd('npx', ['slate-tools',  'deploy', '--skipPrompts'], true);

			await this.afterDeployment();
    }

    console.log(chalk.bgGreen.white('🔥🔥🔥 Deployment successful 🔥🔥🔥'));
  }
}
