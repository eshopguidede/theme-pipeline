const fs = require('fs');
const path = require('path');
const { promisify } = require('util');
const chalk = require('chalk');
const cmd = require('../commands/cmd.js');
const ShopifyHelper = require('../helpers/ShopifyHelper.js');
const dedent = require('dedent-js');
const Themekit = require('@shopify/themekit');
const deepmerge = require('deepmerge');
const writeFileAsync = promisify(fs.writeFile);


module.exports = class Adapter {
  constructor(options) {
    this.options = options;
    this.shopifyHelper = new ShopifyHelper();
  }

  async writeConfig() {
    const configFilePath = path.join(this.configPath, 'settings_data.json');
    const targetTheme = await this.shopifyHelper.getTargetTheme();
    const { branch, skipConfig } = this.options;

    if(skipConfig) {
      console.log('Skipped config step');
      return;
    }

    // only for new themes + master & develop branch
    if(!targetTheme || (targetTheme.role !== 'main' && (targetTheme.isNew !== false || branch.match(/^develop(ment)?|master|main$/)))) {
      const schemaFile = await this.shopifyHelper.fetchFile('config/settings_schema.json');
      const schema = JSON.parse(schemaFile.value);
      const themeInfo = schema.find(obj => obj.name === 'theme_info');

      const localSchema = JSON.parse(fs.readFileSync(path.join(this.configPath, 'settings_schema.json'), 'utf8'));
      const localThemeInfo = localSchema.find(obj => obj.name === 'theme_info');

      if(themeInfo.theme_name !== localThemeInfo.theme_name) {
        console.log('Skipped config step, because the live theme doesn\'t equal the current theme.')
        console.log(`Live theme: ${themeInfo.theme_name}`);
        console.log(`Local theme: ${localThemeInfo.theme_name}`);
        return;
      }

      console.log('Fetch config from live theme...')

      const settings = await this.shopifyHelper.getSettingsData();
      
      await writeFileAsync(configFilePath, settings.value);
    }
	}
	
	async afterDeployment() {
    const { runTests } = this.options;
    const targetTheme = await this.shopifyHelper.getTargetTheme();

    // Create .env, run npm test
		if(runTests) {
      await writeFileAsync("./.env", dedent(`
        THEME_ID=${targetTheme.id}
        SHOPNAME=${this.shopifyHelper.options.shopName}
        BRANCH=${this.options.branch}
      `));

			await cmd('npm', ['test']);
    }
    
    try {
      this.sendNotification();
    } catch(err) {
      // ignore notification exceptions
    }
  }

  async mergeLocales() {
    const files = fs.readdirSync(this.localesPath);

    console.log(chalk.inverse('Merge locales...'));

    for(const file of files) {
      if (file.includes('.json')) {   
        try {
          console.log(chalk.dim(file));

          const oldLocale = JSON.parse(fs.readFileSync(path.join(this.localesPath, file)));
          const liveLocale = await this.shopifyHelper.fetchFile(`locales/${file}`);

          // Merge live locale and locale from repository
          const newLocale = deepmerge(oldLocale, JSON.parse(liveLocale.value));
          
          // Write to file
          fs.writeFileSync(path.join(this.localesPath, file), JSON.stringify(newLocale));
        } catch(err) {
          // Catch 404 responses if new languages are added
        }
      }
    }
  }

  sendNotification() {
    const { slack, branch } = this.options;
    const { shopName } = this.shopifyHelper.options;
    const themeId = this.shopifyHelper.getTargetTheme().id;

    if(slack) {
      const slackClient = require('slack-notify')(slack);

      slackClient.send({
        text: 'Theme Pipeline Deployment Success',
        username: 'Theme Pipeline',
        icon_emoji: ':rocket:',
        fields: {
          Shop: shopName,
          Branch: branch,
          URL: `https://${shopName}.myshopify.com?preview_theme_id=${themeId}`
        }
      });
    }
  }
}