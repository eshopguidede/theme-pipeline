
const fs = require('fs');
const { promisify } = require('util');
const chalk = require('chalk');
const Themekit = require('@shopify/themekit');
const yaml = require('js-yaml');

const writeFileAsync = promisify(fs.writeFile);
const Adapter = require('./Adapter');

const cmd = require('../commands/cmd.js');

module.exports = class ThemekitAdapter extends Adapter {
  
  constructor(options) {
    super(options);
    this.configPath = './config/';
    this.localesPath = './locales';
  }

  async deploy() {
    const { shopNames, apiKeys, passwords, branch } = this.options;

    for(const [idx, name] of shopNames.entries()) {
      console.log(chalk.inverse('Prepare deployment to ' + shopNames[idx]));

      await this.shopifyHelper.init({
        shopName: shopNames[idx],
        apiKey: apiKeys[idx],
        password: passwords[idx],
        branch
      });

      // settings_data.json
      await this.writeConfig();

      // Setup environment config
      const targetTheme = await this.shopifyHelper.getTargetTheme();
      const ignores = fs.existsSync('.themeignore') ? ['.themeignore'] : [];

      fs.writeFileSync('config.yml', yaml.safeDump({
        deployment: {
          password: passwords[idx],
          theme_id: targetTheme.id,
          store: `${shopNames[idx]}.myshopify.com`,
          ignores
        }
      }));

      // locales
      await this.mergeLocales();

      console.log(chalk.bgGreen.white('Deploying to theme #' + targetTheme.id));
      await Themekit.command('deploy', {
        env: 'deployment'
			});

			await this.afterDeployment();
    }

		console.log(chalk.bgGreen.white('🔥🔥🔥 Deployment successful 🔥🔥🔥'));
  }
}
