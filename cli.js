#!/usr/bin/env node
const chalk = require('chalk');
const program = require('commander');
const deploy = require('./commands/deploy');
const test = require('./commands/test');
const cleanup = require('./commands/cleanup');
const package = require('./package.json');
const args = process.argv.slice(2);

// Parse options
program
  .option('-s, --shop-name <type>', 'Shopify Shop Name')
  .option('-k, --api-key <type>', 'Shopify API Key')
  .option('-p, --password <type>', 'Shopify App Password')
  .option('-b, --branch <type>', 'Build Target Branch')
  .option('-m, --mode <type>', 'Build Mode (themekit|slate)')
	.option('-d, --debug', 'Enable debug mode')
	.option('--run-tests', 'Run tests after deployment')
	.option('--slack <type>', 'Slack webhook for build notifications')
	.option('--theme-id <type>', 'Theme ID for automated testing')
  .option('--skip-config', 'Skip config fetching from live theme');

program.parse(process.argv);

// Execute command
switch(args[0]) {
  case 'deploy':
    deploy(program);
		break;
	case 'test':
		test(program);
    break;
  case 'version':
    console.log(chalk.bold(`Eshop Guide Pipeline™ 🚀 Version ${package.version}`));
    break;
  case 'cleanup':
    cleanup(program);
    break;
  default:
    console.log(chalk.bgRed.white(`Unknown command "${args[0]}"`));
}