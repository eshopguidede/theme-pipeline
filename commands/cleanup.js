const chalk = require('chalk');
const ShopifyHelper = require('../helpers/ShopifyHelper.js');

module.exports = async function(program) {
  const { shopName, apiKey, password, branch } = program;
  let targetName;

  // Validation
  if(!shopName) { console.log(chalk.bgRed.white('Invalid shopname')); return; }
  if(!apiKey) { console.log(chalk.bgRed.white('Invalid API key')); return; }
  if(!password) { console.log(chalk.bgRed.white('Invalid password')); return; }
  if(!branch) { console.log(chalk.bgRed.white('Invalid branch')); return; }

  // Prepare deployment to multi-stores
  const shopNames = shopName.split(';;;'),
        apiKeys = apiKey.split(';;;');
        passwords = password.split(';;;');

  if(shopNames.length !== apiKeys.length || shopNames.length !== passwords.length) {
    throw new Error(`Invalid configuration: Different lengths for SHOPNAME (${shopNames.length}), API_KEY (${apiKeys.length}) and API_PASSWORD (${passwords.length})`);
  } else {
    const options = { shopNames, apiKeys, passwords, branch };
    
    for await(const [idx, name] of shopNames.entries()) {
      console.log(chalk.inverse(`Cleanup branch ${branch} in ${shopNames[idx]}`));

      const shopifyHelper = new ShopifyHelper();

      await shopifyHelper.init({
        shopName: shopNames[idx],
        apiKey: apiKeys[idx],
        password: passwords[idx],
        branch
      });

      // Remove old Release Candidates
      if(branch === 'master' || branch === 'main') {
        const releaseCandidatePattern = /Release Candidate \d{2}.\d{2}.\d{4} - \d{2}:\d{2}/;
        const { targetName, themesList } = shopifyHelper;

        if(targetName.match(releaseCandidatePattern)) {
          for await(theme of themesList) {
            if (theme.name.match(releaseCandidatePattern) && theme.role != 'main') {
              await shopifyHelper.deleteTheme(theme.id);
              console.log(chalk.bgGreen.white(`Deleted theme "${theme.name}"`));
            }
          }
        }
      } else {
        const targetTheme = await shopifyHelper.getTargetTheme(false);

        if(targetTheme) {
          if(targetTheme.role === 'main') {
            console.log(chalk.bgRed.white(`Live theme "${targetTheme.name}" can't be deleted!`));
          } else if(branch.match(/^develop(ment)?|master$/)) {
            console.log(chalk.bgRed.white(`Theme "${targetTheme.name}" won't be deleted!`));
          } else {
            await shopifyHelper.deleteTheme(targetTheme.id);
            console.log(chalk.bgGreen.white(`Deleted theme "${targetTheme.name}"`));
          }
        } else {
          console.log(chalk.bgRed.white(`Failed to cleanup branch ${branch}`));
        }
      }
    }
  }
}
