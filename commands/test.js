const fs = require('fs');
const chalk = require('chalk');
const { promisify } = require('util');
const cmd = require('../commands/cmd.js');

const readFileAsync = promisify(fs.writeFile);
const writeFileAsync = promisify(fs.writeFile);

module.exports = async function(program) {
	const { shopName, themeId } = program;

  // Validation
	if(!shopName) { console.log(chalk.bgYellow.white('Warning: Invalid shopname. Defaults from cypress.json will be used.')); }
	if(!themeId) { console.log(chalk.bgYellow.white('Warning: Invalid theme id. Defaults from cypress.json will be used.')); }

	if(!!shopName && !!themeId) {
		await writeFileAsync('./cypress.json', JSON.stringify({
			baseUrl: `https://${shopName}.myshopify.com`,
			watchForFileChanges: false,
			env: {
				'THEME_ID': themeId
			}
		}));
	}

	await cmd('npx', ['cypress',  'run']);
}
