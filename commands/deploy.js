const chalk = require('chalk');
const ThemekitAdapter = require('../adapters/Themekit');
const SlateAdapter = require('../adapters/Slate');
const SlateV0Adapter = require('../adapters/SlateV0');
const cleanup = require('../commands/cleanup');

module.exports = async function(program) {
  const { shopName, apiKey, password, branch, mode, debug, runTests, slack, skipConfig } = program;
  let targetName;

  // Validation
  if(!shopName) { console.log(chalk.bgRed.white('Invalid shopname')); return; }
  if(!apiKey) { console.log(chalk.bgRed.white('Invalid API key')); return; }
  if(!password) { console.log(chalk.bgRed.white('Invalid password')); return; }
  if(!branch) { console.log(chalk.bgRed.white('Invalid branch')); return; }
  if(!mode || (!['slate', 'slatev0', 'themekit'].includes(mode))) { console.log(chalk.bgRed.white('Invalid mode')); return; }

  // Prepare deployment to multi-stores
  const shopNames = shopName.split(';;;'),
        apiKeys = apiKey.split(';;;');
        passwords = password.split(';;;');

  

  if(shopNames.length !== apiKeys.length || shopNames.length !== passwords.length) {
    throw new Error(`Invalid configuration: Different lengths for SHOPNAME (${shopNames.length}), API_KEY (${apiKeys.length}) and API_PASSWORD (${passwords.length})`);
  } else {
    const options = { shopNames, apiKeys, passwords, branch, mode, debug, runTests, slack, skipConfig };
    let adapter;

    // Remove old release candidates
    if(branch === 'master' || branch === 'main') {
      await cleanup(program);
    }

    switch(mode.toLowerCase()) {
      case 'slate':
        adapter = new SlateAdapter(options);
        break;
      case 'slatev0':
        adapter = new SlateV0Adapter(options);
        break;
      default:
        adapter = new ThemekitAdapter(options);
    }

    adapter.deploy();
  }
}
