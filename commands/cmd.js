const { spawn } = require('child_process');
const readline = require('readline');

module.exports = function cmd(name, params, clearLine = false) {
  return new Promise(function (resolve, reject) {
    var cmd = spawn(name, params);
    var lastStdout = '';

    cmd.stdout.on('data', function (data) {
      var msg = data.toString();

      if(clearLine && readline) {
        readline.clearLine(process.stdout);
        readline.cursorTo(process.stdout, 0);
        process.stdout.write(msg);
      } else {
        console.log(msg);
      }
    });
  
    cmd.stderr.on('data', function (data) {
      console.error(data.toString());
    });
  
    cmd.on('exit', function (code) {
      const msg = 'child process exited with code ' + code.toString();
      
      if(code !== 0) {
        throw new Error(msg);
      }

      resolve(code);
    });
  });
}