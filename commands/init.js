const chalk = require('chalk');
const fs = require('fs');
const path = require('path');
const package = require('../package.json');

const dir = process.env.INIT_CWD || process.cwd();
const configPath = path.join(dir, '.github/workflows');
const mainYml = path.join(configPath, 'main.yml');
const cleanupYml = path.join(configPath, 'cleanup.yml');

if (!fs.existsSync(configPath)) {
  fs.mkdirSync(configPath, { recursive: true });
}

if (!fs.existsSync(mainYml)) {
  fs.copyFileSync('./main-template.yml', mainYml);
  console.log(`Created ${mainYml}`);
}

if (!fs.existsSync(cleanupYml)) {
  fs.copyFileSync('./cleanup-template.yml', cleanupYml);
  console.log(`Created ${cleanupYml}`);
}

console.log(chalk.bgGreen.white.bold('\nSuccessfully installed Eshop Guide Pipeline™ 🚀\n'));
console.log(chalk.dim(`Version ${package.version}\n`));
console.log('For more information, visit https://writeaguide.com/guides/eshop-guide-theme-pipeline-font7irw8zppr5ni/view');
