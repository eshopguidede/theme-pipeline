const Shopify = require('shopify-api-node');
const dateFormat = require("dateformat");
const chalk = require('chalk');

module.exports = class ShopifyHelper {
  async init(options) {
		this.options = options;
		this.shopify = new Shopify(options);
    this.themesList = await this.shopify.theme.list();
    this.targetName = await this.getTargetName(options.branch);
    this.publishedId = this.themesList.find(theme => theme.role === 'main').id;
    this.targetTheme = this.themesList.find((theme) => theme.name === this.targetName);
  }

  async getSettingsData() {
    return await this.fetchFile('config/settings_data.json');
  }

  async fetchFile(key) {
    if(!this.publishedId) {
      await this.init();
    }

    return await this.shopify.asset.get(this.publishedId, {
      asset: {
        key
      }
    });
  }

  async getFormattedDate() {
    const shop = await this.shopify.shop.get();
    const iana_timezone = shop.iana_timezone;
    const localeDate = new Date(new Date().toLocaleString("en-US", {timeZone: iana_timezone}));
    const formattedDate = dateFormat(localeDate, 'dd.mm.yyyy - HH:MM');
    return formattedDate;
  }

  async getTargetName(branch) {
    if(this.targetName) {
      return this.targetName;
    }
    const date = await this.getFormattedDate();
    switch (branch.toLowerCase()) {
      case 'master':
      case 'main':
        this.targetName = `Release Candidate ${date}`;
        break;
      case 'development':
      case 'develop':
        this.targetName = 'PREVIEW – DO NOT CHANGE';
        break;
      default:
        this.targetName = branch;
    }
    return this.targetName;
  }

  async getTargetTheme(createIfNotExisting = true) {
    if(!this.targetTheme && createIfNotExisting) {
      this.targetTheme = await this.shopify.theme.create({
        name: this.targetName
      });
    }

    return this.targetTheme;
  }

  async deleteTheme(id) {
    await this.shopify.theme.delete(id);
  }
}